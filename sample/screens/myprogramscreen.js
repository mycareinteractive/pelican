import Pelican from 'pelican';
require('../css/edtitles.css');
var template = require('../templates/myprogramscreen.hbs');

const MyProgramScreen = Pelican.Screen.extend({

    className: 'myprogram edtitles',

    template: template,

    keyEvents: {},

    events: {
        'click .back-button': 'back'
    },

    widgets: {
        bookmarks: {
            widgetClass: Pelican.ContentButtonGroup,
            selector: '#bookmarks',
            options: {nonActive: true, backButton: true}
        }
    },

    onInit: function (options) {
        /*
         // TODO: remove test logic
         var bookmarks = require("../testdata/bookmarks.json");
         var contents = App.dataStore.BookmarksToContents(bookmarks);
         var model = {buttons: contents};
         this.getWidget('bookmarks').model.set(model);
         */
        var slug = App.upserver.baseSlug + "me/bookmarks";
        console.log('Slug: ' + this.path);

        var self = this;

        App.Bookmarks.fetch()
            .done(function () {
                console.log('APP ROUTE SEGMENT: ' + slug);
                var bmark = App.Bookmarks.toJSON();
                var bmod = {buttons: bmark};
                self.getWidget('bookmarks').model.set(bmod);
                self.getWidget('bookmarks').selectFirst();
            });
    },

    onAttach: function () {
        this.getWidget('bookmarks').selectFirst();
    }
});

export default MyProgramScreen;