import Pelican from 'pelican';
require('../css/edtitles.css');
var template = require('../templates/edlibraryscreen.hbs');

const EdLibraryScreen = Pelican.Screen.extend({

    className: 'edlibrary edtitles',

    template: template,

    keyEvents: {},

    events: {
        'click .back-button': 'back',
        'click .folder': 'selectFolder'
    },

    widgets: {
        library: {
            widgetClass: Pelican.ContentMenu,
            selector: '#menu',
            options: {}
        }
    },

    onInit: function (options) {
        var slug = this.path.replace('home/', App.upserver.baseSlug);
        console.log('Slug: ' + this.path);

        var self = this;

        //App.Content = new Pelican.Models.Content();
        App.Contents.fetch({
            data:{
                slug: slug,
                depth: 2
            }
        })
            .done(function(){
                console.log('APP ROUTE SEGMENT: ' + slug);
                var content = App.Contents.toJSON();

                //App.dataStore.patchContent(content);
                var model = {menu: content};
                self.getWidget('library').model.set('folders', content);
            });
    },

    onAttach: function () {
        this.getWidget('library').selectFirstFolder();
    },

    selectFolder: function(e) {
        var children = null;
        var $obj = $(e.target);
        var slug = $obj.attr('data-slug');

        var self = this;

        //App.Content = new Pelican.Models.Content();
        App.Contents.fetch({
            data:{
                slug: slug,
                depth: 2
            }
        })
            .done(function(){
                var content = App.Contents.toJSON();

                //App.dataStore.patchContent(content);
                self.getWidget('library').model.set('assets', content);
            });
    }
});

export default EdLibraryScreen;