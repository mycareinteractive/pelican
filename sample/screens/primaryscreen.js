import Pelican from 'pelican';
import i18next from 'i18next';

require('../css/primaryscreen.css');
require('../css/dialog.css');

var template = require('../templates/primaryscreen.hbs');

const PrimaryScreen = Pelican.Screen.extend({

    className: 'primary',

    template: template,

    keyEvents: {
        'POWER': true
    },

    events: {
        'click #my-language': 'languageSelect'
    },

    widgets: {
        datetime: {
            widgetClass: Pelican.DigitalClock,
            selector: '#date',
            options: {format: 'hh:MM TT, dddd, mmmm d, yyyy', locale: 'en'}
        },
        mainmenu: {
            widgetClass: Pelican.DropdownMenu,
            selector: '#menu'
        },
        dialog: {
            widgetClass: Pelican.Dialog,
            selector: '#dialog',
            options: {hidden: true}
        }
    },

    onInit: function (options) {
        var slug = App.config.baseSlug.substr(0, App.config.baseSlug.length - 1);
        var self = this;

        App.Contents.fetch({
            data: {
                slug: slug,
                depth: 2
            }
        })
            .done(function () {
                console.log('APP ROUTE SEGMENT: ' + slug);
                var content = App.Contents.toJSON();
                var model = {menu: content};
                self.getWidget('mainmenu').model.set(model);
                self.getWidget('mainmenu').click('#menu1');
            });
    },

    onAttach: function () {
        this.getWidget('mainmenu').click('#menu1');
    },

    languageSelect: function (e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        var p1 = 'Select a Language:';
        var p2 = 'Translations are provided wherever possible. Not all content has been translated. If you need additional help, please talk to your clinical staff.';
        var p3 = 'For help, ask a member of your care team or press the Nurse button.';

        var html =
            '<div class="english">' +
            '<p class="text4">' + p1 + '</p><br>' +
            '<p class="text2"><br>' + p2 + '<br><br></p>' +
            '<p class="text1">' + p3 + '</p>' +
            '</div>' +
            '<div class="spanish" style="display: none">' +
            '<p class="text4">' + i18next.t(p1, {lng: 'es'}) + '</p><br>' +
            '<p class="text2"><br>' + i18next.t(p2, {lng: 'es'}) + '<br><br></p>' +
            '<p class="text1">' + i18next.t(p3, {lng: 'es'}) + '</p>' +
            '</div>';

        var model = {
            buttons: [
                {id: 'english', text: 'English'},
                {id: 'spanish', text: 'Spanish'}
            ],
            content: html
        };
        this.getWidget('dialog').show();
        this.getWidget('dialog').model.set(model);
        this.focus('#english');
    }
});

export default PrimaryScreen;