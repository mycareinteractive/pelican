import Pelican from 'pelican';

require('../css/secondaryscreen.css');
var template = require('../templates/secondaryscreen.hbs');

const SecondaryScreen = Pelican.Screen.extend({

    className: 'secondary',

    template: template,

    keyEvents: {},

    events: {
        'click .back-button': 'back',
    },

    widgets: {
        menu: {
            widgetClass: Pelican.TabMenu,
            selector: '#menu'
        }
    },

    onInit: function (options) {
        var slug = this.path.replace('home/', App.upserver.baseSlug);
        console.log('Slug: ' + this.path);

        var self = this;

        // get self content
        var parent = new Pelican.Models.Content();
        parent.fetch({
            data: {
                slug: slug
            }
        }).done(function () {
            var pcont = parent.toJSON();
            self.$('.page-title').first().html(pcont.content.name);
            self.getWidget('menu').model.set({parent: pcont.content});
        });

        var children = new Pelican.Collections.ContentCollection();
        children.fetch({
            data: {
                slug: slug,
                depth: 2
            }
        })
            .done(function () {
                console.log('APP ROUTE SEGMENT: ' + slug);
                var content = children.toJSON();
                var model = {menu: content};
                self.getWidget('menu').model.set(model);
                self.focus(self.$('.back-button'));
            });

    },

    onAttach: function () {
        this.getWidget('menu').click('#menu1');
    }
});

export default SecondaryScreen;