import SecondaryScreen from './secondaryscreen';

const MyChartScreen = SecondaryScreen.extend({

    keyEvents: function () {
        return {};
    },

    events: function () {
        return $.extend(SecondaryScreen.prototype.events, {
            'click [data-current-slug="information"]': 'myChartVideo'
        });
    },

    myChartVideo: function () {
        console.log('launch mychart video');
    }

});

export default MyChartScreen;
