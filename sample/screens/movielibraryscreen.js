import Pelican from 'pelican';
require('../css/movielibraryscreen.css');
var template = require('../templates/movielibraryscreen.hbs');

const MovieLibraryScreen = Pelican.Screen.extend({

    className: 'movielibrary',

    template: template,

    keyEvents: {},

    events: {
        'click .back-button': 'back',
        'click .folder': 'selectFolder'
    },

    widgets: {
        library: {
            widgetClass: Pelican.ContentCarousel,
            selector: '#menu',
            options: {}
        }
    },

    onInit: function (options) {
        /*
         // TODO: remove test logic
         var content = require("../testdata/contents.json");
         App.dataStore.patchContent(content);
         this.getWidget('library').model.set('folders', content[2].children[0].children);
         */

        var slug = this.path.replace('home/', App.upserver.baseSlug);
        console.log('Slug: ' + this.path);

        var self = this;

        App.Contents.fetch({
            data: {
                slug: slug,
                depth: 2
            }
        })
            .done(function () {
                console.log('APP ROUTE SEGMENT: ' + slug);
                var content = App.Contents.toJSON();

                //App.dataStore.patchContent(content);
                self.getWidget('library').model.set('folders', content);
            });
    },

    onAttach: function () {
        this.getWidget('library').selectFirstFolder();
    },

    selectFolder: function (e) {
        /*
         // TODO: remove test logic
         var $obj = $(e.target);
         var children = null;
         var content = require("../testdata/contents.json");
         App.dataStore.patchContent(content);
         $.each(content[2].children[0].children, function(i, folder) {
         if(folder.slug == $obj.attr('data-slug')) {
         children = folder.children;
         return false;
         }
         });
         */

        var $obj = $(e.target);
        var slug = $obj.attr('data-slug');

        var self = this;

        var assets = new Pelican.Collections.ContentCollection();
        assets.fetch({
            data: {
                slug: slug,
                depth: 1
            }
        })
            .done(function () {
                var content = assets.toJSON();

                //App.dataStore.patchContent(content);
                self.getWidget('library').model.set('assets', content);
            });
    }
    // set "assets" model attribute, this will trigger ContentMenu to update asset list
    // this.getWidget('library').model.set('assets', children);
});

export default MovieLibraryScreen;