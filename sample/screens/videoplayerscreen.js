import Pelican from 'pelican';

require('../css/video.css');
var template = require('../templates/videoplayerscreen.hbs');

const VideoPlayerScreen = Pelican.Screen.extend({

    className: 'video',
    template: template,

    video: new Pelican.Models.Content(),
    vid: 0,

    state: 'stop',
    language: 'en',

    autoStart: true,
    bookmarking: true,
    trickModes: true,
    analytics: true,
    startPosition: 0,

    startThreshold: 10000,
    stopThreshold: 10000,

    width: 1280,
    height: 720,
    posX: 0,
    posY: 0,

    keyEvents: {
        'FFWD': 'fastForward',
        'RIGHT': 'fastForward',
        'LEFT': 'rewind',
        'RWND': 'rewind',
        'PAUSE': 'playPause',
        'PLAY': 'playPause',
        'INFO': 'stop',
        'STOP': 'stop',
        'UP': 'stop'
    },

    events: {},
    widgets: {},

    getVideo: function(videoId){
        var self = this;
        var defer = jQuery.Deferred();

        var vid = videoId || self.vid;

        self.video.fetch({
            uri: '/contents',
            data:{
                id: vid
            }
        })
            .done(function(){
                self.state = 'loaded';
                defer.resolve();
            })
            .fail(function(f){
                defer.reject(f.errorMessage);
            });

        return defer.promise();
    },

    getMediaUrl: function(){
        var self = this;

        var obj = self.video.toJSON();
        var vobj = obj.renditions[0];
        var vurl = vobj.metadatas.url || vobj.metadatas.originalUrl;

        console.log('MEDIA OBJECT');
        console.log(obj);

        return vurl;
    },

    stop: function(){
        var self = this;
        if(self.state == 'stop') {
            return false;
        }

        $.doTimeout('queue');
        console.log('We pressed the stop button');
        App.device.stopMedia()
            .done(function(){
                self.state = 'stop';
            });
        self.onStop();
    },

    seek: function(timeInMs){
        var defer = jQuery.Deferred();
        App.device.setMediaPosition(timeInMs)
            .done(function(){
                console.log("The movie has changed its position to: " + timeInMs);
            });
        return defer.promise();
    },

    startVideo: function(){
        var self = this;
        var defer = jQuery.Deferred();

        var vurl = self.getMediaUrl();
        console.log(vurl);

        App.device.playMedia({
            url: vurl
        })
            .done(function(){
                defer.resolve();
                console.log('WE HAVE SUCCESSFULLY STARTED THE VIDEO');
            })
            .fail(function(f){
                defer.reject(f.errorMessage);
                console.log('MOVIE PLAY HAS FAILED: ' + f.errorMessage)
            });

        return defer.promise();
    },

    playPause: function(){
        var self = this;
        if(self.state == 'play') {
            App.device.pauseMedia()
                .done(function(){
                    self.state = 'pause';
                });
        }
        else {
            $.doTimeout('queue');
            App.device.resumeMedia()
                .done(function(){
                    self.state = 'play';
                });
        }
    },

    fastForward: function(){
        if (!this.trickModes)
            return false;

        var self = this;
        var defer = jQuery.Deferred();
        console.log('You are pressing FFWD.');

        if(self.state == 'fastForward'){
            $.doTimeout('queue');
            App.device.resumeMedia()
                .done(function(){
                    self.state = 'play';
                });
        }
        else {
            $.doTimeout('queue', 500, function(){
                App.device.getMediaPosition()
                    .done(function (positionInMs) {
                        defer.resolve();
                        App.device.setMediaPosition(positionInMs + 10000);
                        self.state = 'fastForward';
                    })
                    .fail(function (f) {
                        defer.reject(f.errorMessage);
                        console.log('We were not able to set the Media Position');
                    });
                return true;
            });
        }

        return defer.promise();
    },

    rewind: function(){
        if (!this.trickModes)
            return false;

        var self = this;
        var defer = jQuery.Deferred();
        console.log('You are pressing REWIND.');

        if(self.state == 'rewind'){
            $.doTimeout('queue');
            App.device.resumeMedia()
                .done(function(){
                    self.state = 'play';
                });
        }
        else {
            $.doTimeout('queue', 500, function() {
                App.device.getMediaPosition()
                    .done(function (positionInMs) {
                        defer.resolve();
                        App.device.setMediaPosition(positionInMs - 10000);
                        self.state = 'rewind';
                    })
                    .fail(function (f) {
                        defer.reject(f.errorMessage);
                        console.log('We were not able to set the Media Position');
                    });
                return true;
            });
        }

        return defer.promise();
    },

    onInit: function (options) {
        var self = this;
        console.log('THESE ARE THE OPTIONS');
        console.log(options);

        $('body').addClass('video');

        if (self.params.length > 1) {
            self.vid = self.params[0];
        }

        if (self.queries && self.queries['vid']) {
            self.vid = self.queries['vid'];
        }

        // self.vid = "razuna-vid-4CB434FBD33D472D9712FB13CA70F0BD"; //educational
        // self.vid = "razuna-vid-8D17FFC03CB9468ABF48CD9C9964C263"; //glab educational
        // self.vid = "razuna-vid-278473DCB1184A64AF4963A2E4FDD5EE"; // SD education
        // this.vid = "razuna-vid-E9A96B8BF264451B81EFA378071C6AE5"; //swank

        self._initializeMediaEvents();

        self.getVideo()
            .done(function(){
                self.startVideo();
            });

        console.log('PELICAN PLATFORM: ' + App.device.getPlatform());
        console.log('Initialize FETCH for video data');
    },

    onScreenShow: function(){
        console.log('onScreenShow: Showing the Video PLayer screen');
        $('body').addClass('video');
    },

    onScreenHide: function(){
        console.log('onScreenHide: Hiding the Video Player screen');
        this.stop();
        $('body').removeClass('video');
    },

    /*
    * The methods below are ment to be overriden and are used to hook into
    * the media players native callback functions.
    * */

    onPlayStart: function(){},
    onPlayEnd: function(){},
    onStop: function(){ this.back(); },
    onErrorInPlaying: function(){},
    onBufferFull: function(){},
    onFileNotFound: function(){},
    onNetworkDisconnected: function(){},
    onNetworkBusy: function(){},
    onNetworkCannotProcess: function(){},
    onSeekDone: function(){},

    /**
     * PRIVATE METHODS
     */
    _initializeMediaEvents: function(){
        var self = this;

        document.addEventListener('media_event_received', function(param){
            switch(param.eventType) {
                case 'play_start':
                    console.log('EVENT: play_start -- The video has started to play.');
                    if(self.state == 'loaded' && self.startPosition > 0) {
                        self.seek(self.startPosition)
                            .done(function(){
                                self.state = 'play';
                            });
                    }
                    self.onPlayStart();
                    break;
                case 'play_end':
                    self.stop();
                    console.log('EVENT: play_end -- The video has ended.');
                    self.onPlayEnd();
                    break;
                case 'error_in_playing':
                    self.stop();
                    console.log('EVENT: error_in_playing -- There jhas been an error in play back.');
                    self.onErrorInPlaying();
                    break;
                case 'buffer_full':
                    self.stop();
                    console.log('EVENT: buffer_full -- The buffer is full.');
                    self.onBufferFull();
                    break;
                case 'file_not_found':
                    console.log('EVENT: file_not_found -- The file could not be found.');
                    self.onFileNotFound();
                    break;
                case 'network_disconnected':
                    console.log('EVENT: network_disconnected -- The network as disconnected.');
                    self.onNetworkDisconnected()
                    break;
                case 'network_busy':
                    console.log('EVENT: network_busy -- The network is currently busy.');
                    self.onNetworkBusy();
                    break;
                case 'network_cannot_process':
                    console.log('EVENT: network_cannot_process -- The network cannot process.');
                    self.onNetworkCannotProcess();
                    break;

                /*
                * TODO: Cant do seek_done yet because of how we are handling ff/rwd trick modes
                * We are essentially executing multiple seeks to mimic ff/rwd
                * This will also interfere with setPlayPosition
                * To maintain simplicity, we should avoid using this at all cost.
                 */
                /* case 'seek_done':

                    console.log('EVENT: seek_done -- The seek has finished');

                    if(this.state == 'fastForward') {
                        App.device.stopMedia()
                            .always(function () {
                                // After video stops, oddly the TV will start playing
                                App.device.stopChannel();
                                self.state = 'stop';
                            });
                    } else {
                        App.device.resumeMedia()
                            .done(function(){
                                self.state = 'play';
                            });
                    }

                    self.onSeekDone();
                    break;
                */
            }

        }, false);
    },

    _trackEvent: function (action) {
        // TODO: Where is 'asset' value coming from
        var label = this.asset.tagAttribute.localEntryUID;
        if (this.asset.ListOfMetaData) {
            label += '|' + this.asset.ListOfMetaData.Title;
        }
        var options = {};
        options['nonInteraction'] = true;
        nova.tracker.event(this.type, action, label, 0, options);
    }
});

export default VideoPlayerScreen;