import Pelican from 'pelican';

require('../css/introscreen.css');
var template = require('../templates/introscreen.hbs');

const IntroScreen = Pelican.Screen.extend({

    className: 'intro',

    template: template,

    keyEvents: {
        'POWER': true,
        'MUTE': 'toggleMute'
    },

    events: {
        'click #watchvideo': 'playWelcomeVideo',
        'focus #watchvideo': 'highlightWelcomeVideo'
    },

    widgets: {
        buttons: {
            widgetClass: Pelican.ButtonGroup,
            selector: '#buttons',
            options: { orientation: 'vertical', preRender: true }
        }
    },

    toggleMute: function() {

    },

    playWelcomeVideo: function() {
        this.log('play welcome video');
    },

    highlightWelcomeVideo: function() {
        this.log('welcome video highlighted');
    }
});

export default IntroScreen;