// agent-shim is needed to make sure Marionette Inspector works in Chrome
import upserver from 'upserver-client';
import Pelican from 'pelican';
import i18next from 'i18next';
import XHR from 'i18next-xhr-backend';
import './helpers/agent-shim';
import '../src/helpers/backbone.sync.override';
import Router from './router';
import './templates/primaryscreen.hbs';

// If test on PC use default desktop from Pelican
if (navigator.userAgent.indexOf(';LGE ;') > -1) {
    //var Procentric = require('pelican-procentric');
    //window.PelicanDevice = new Procentric();
    //console.log("I think we are Procentric");
}
else {
    window.PelicanDevice = new Pelican.Desktop();
    console.log("We are just a humble PC");
}

require('./css/index.css');

// Instantiate App and main players
const App = window.App = new Pelican.Application({
    router: new Router(),
    layout: new Pelican.StackLayout(),
    tracker: new Pelican.Tracker()
});

App.device = PelicanDevice;

// dataStore handles data to/from server
App.Contents = new Pelican.Collections.ContentCollection();
App.Bookmarks = new Pelican.Collections.BookmarkCollection();

App.upserver = upserver;

// render the app
App.screens = App.layout.screens; // a shorthand to access all screens
App.layout.render('#app');

// Override this function when you need to do something synchronously before app start
App.onBeforeStart = function () {
    console.log('App before start');
};

App.data = {};

// Override this function when you need to do something asynchronously before app start
// Call the this.start() when you are all set.
App.onConfigReady = function () {
    var self = this;
    var config = App.config;

    console.log('Config ready');
    console.log('Beam me up, Scotty...');

    App.upserver = upserver;
    App.upserver.baseSlug = config.baseSlug;

    // init i18n
    i18next
        .use(XHR)
        .init({
            lng: 'en',
            debug: true,
            fallbackLng: "en",
            nsSeparator: false,
            keySeparator: false,
            backend: {
                "loadPath": "locales/{{lng}}/{{ns}}.json"
            }
        }, function (err, t) {

            // init upserver client
            App.upserver.init({
                host: config.server,
                clientId: config.appId,
                deviceId: '0021F8034F72'
            })
                .done(function () {
                    console.log('server authentication ok');
                    if (upserver.signalR.enabled) {
                        console.log('connected to signalR');
                    }

                    App.upserver.api('/device').always(function (device) {
                        App.data.device = device;

                        App.upserver.api('/me').always(function (patient) {
                            App.data.patient = patient;

                            App.tracker.init(
                                config.analyticsProvider,
                                config.analyticsUrl,
                                config.analyticsSite,
                                App.data.device.deviceProvision.location.roomBed
                            );

                            if(patient && patient.patientVisit) {
                                App.tracker.setUserId(patient.patientVisit.id);
                            }

                            // Start the app
                            self.start();
                        });
                    });

                })
                .fail(function () {
                    console.log('server authentication failed');
                });
        });
};

window.app = App;

// Kick off everything
App.bootstrapDevice();

export default App;

