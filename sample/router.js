import $ from 'jquery';
import Backbone from 'backbone';
import Pelican from 'pelican';

const Router = Pelican.Router.extend({

    // automatic screen routes
    appRoutes: {
        '': '',
        'intro': 'IntroScreen',
        'agreement': 'AgreementScreen',
        'videoplayer/:id': 'VideoPlayerScreen',
        'home': 'PrimaryScreen',
        'home/my-care/my-education': 'MyProgramScreen',
        'home/my-care/health-education-library': 'EdLibraryScreen',
        'home/entertainment/free-movies': 'MovieLibraryScreen',
        'home/visit/visitor-guide/restaurants': 'SecondaryScreen',
        'home/my-care/mychart': 'MyChartScreen',
        'home/:group/:id': 'SecondaryScreen',
        'movie/:vid': 'MovieSynopsisScreen',
        'education/:vid': 'EducationSynopsisScreen'
    },

    // To avoid writing too many switch/case, we let webpack pack everything in ./screens/ and require them dynamically.
    // This function will be called during routing check.  Implement this function so it reflects to your screen folder
    // structure.
    getScreen: function(name) {
        try {
            var ScreenClass = require('./screens/' + name.toLowerCase() + '.js').default;
        }
        catch (e) {
            console.log('Screen not found. ' + e.toString());
            return undefined;
        }
        return ScreenClass;
    },

    // this function will be called right after router started
    onStart: function () {
        // Add your startup redirecting logic here
        // Eg: if patient is discharged go to discharge page, otherwise go to agreement page or home page
        // Eg: if the URL is provided (deep linking), maybe do nothing
        var startPath = this.getFullPath();
        if (!startPath || startPath.length < 2)
            this.go('intro');
        console.log('landing route:' + this.getFullPath());
    }

});

export default Router;