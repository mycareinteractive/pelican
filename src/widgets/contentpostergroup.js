import Widget from './widget';

var template = require('../templates/contentpostergroup.hbs');

/**
 * ContentPosterGroup is similar to ContentButtonGroup but displays images instead.
 */
const ContentPosterGroup = Widget.extend({
    className: 'content-poster-group',
    template: template,

    // self properties
    backButton: false,
    nonActive: false,
    orientation: 'horizontal',
    contentClass: '',
    contentUrlPrefix: 'movie',

    // cached jQuery objects
    $carouselTitle: $(),

    events: {
        'focus .poster': 'focusPoster'
    },

    templateHelpers: function () {
        return {
            backButton: this.backButton,
            contentClass: this.contentClass,
            contentUrlPrefix: this.contentUrlPrefix
        }
    },

    onRender: function () {
        this.$carouselTitle = this.$('.carouselTitle p');

        if (this.nonActive) {
            this.$el.addClass('nonactive');
        }

        if (this.orientation == 'horizontal') {
            this.$('.carouselPosters').css('overflow-x', 'scroll');
        }
        else {
            this.$('.carouselPosters').css('overflow-y', 'scroll');
        }
    },

    onAttach: function () {
        this.$el.css('overflow', 'hidden');
    },

    onKey: function (e, key) {
        var direction = '', handled = false;

        // navigation keys
        if (['UP', 'DOWN', 'LEFT', 'RIGHT'].indexOf(key) >= 0) {
            // orientation defaults to vertical
            if (this.orientation == 'horizontal') {
                if (key == 'LEFT')
                    direction = 'prev';
                else if (key == 'RIGHT')
                    direction = 'next';
            }
            else {
                if (key == 'UP')
                    direction = 'prev';
                else if (key == 'DOWN')
                    direction = 'next';
            }

            if (direction) {
                handled = true;
                this.moveFocus(direction, this.$el);
            }
        }
        else if (key == 'PGUP' || key == 'PGDN') {
            handled = true;
            direction = (key == 'PGUP') ? 'prev' : 'next';
            this.scrollPage(direction, this.$el, 'a');
        }

        this.log('key:', key, ', handled:', handled);
        return handled;
    },

    focusPoster: function (e) {
        var $obj = $(e.target);
        $obj.siblings().removeClass('active');
        $obj.addClass('active');
        this.setTitle($obj.attr('title'));
    },

    setTitle: function(title) {
        this.$carouselTitle.text(title);
    }
});

export default ContentPosterGroup;