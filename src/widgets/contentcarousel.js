import Widget from './widget';
import ContentButtonGroup from './contentbuttongroup';
import ContentPosterGroup from './contentpostergroup';
import i18n from 'i18next';

var template = require('../templates/contentcarousel.hbs');

/**
 * ContentCarousel is very similar to ContentMenu widget but optimal for displaying asset posters instead of asset titles.
 * ContentMenu listens to two model attribute changes: "folders" and "assets"
 */
const ContentCarousel = Widget.extend({
    className: 'content-carousel',
    template: template,

    // self properties

    // cached jQuery objects
    $folders: $(),
    $carousel: $(),

    widgets: {
        folders: {
            widgetClass: ContentButtonGroup,
            selector: '.major',
            options: {backButton: true, nonActive: true, contentClass: 'folder'}
        },
        assets: {
            widgetClass: ContentPosterGroup,
            selector: '#carousel',
            options: {backButton: false, nonActive: false, contentClass: 'asset', orientation: 'horizontal'}
        }
    },

    modelEvents: {
        'change:folders': 'foldersChanged',
        'change:assets': 'assetsChanged'
    },

    events: {
        'click .folder': 'selectFolder',
        'click .leftarrow': 'assetLeft',
        'click .rightarrow': 'assetRight'
    },

    templateHelpers: function () {
        return {}
    },

    onRender: function () {
        this.$('.carouselPosters').css('overflow-x', 'scroll');
    },

    onAttach: function () {
        this.$folders = this.$('.major a');
        this.$carousel = this.$('#carousel');
    },

    onKey: function (e, key) {
        var handled = false;

        if (key == 'LEFT' || key == 'RIGHT' || key == 'PGUP' || key == 'PGDN') {
            // rotate carousel
            handled = true;
            this.getWidget('assets').onKey(e, key);
        }
        else if (key == 'UP' || key == 'DOWN') {
            // nav inside folders
            handled = true;

            this.getWidget('folders').onKey(e, key);
            // folders automatically gets a click if nav using remote
            var $nextButton = this.$folders.filter('.selected');

            if ($nextButton.hasClass('folder')) {
                this.$carousel.show();
                this.click($nextButton);
            }
            else {
                // back-button
                this.$carousel.hide();
                this.model.set('assets', null);
            }
        }

        this.log('key:', key, ', handled:', handled);
        return handled;
    },

    foldersChanged: function (model, folders) {
        this.getWidget('folders').model.set('buttons', folders);
        this.getWidget('assets').model.clear();
        this.$folders = this.$('.folder');
    },

    assetsChanged: function (model, assets) {
        var t = i18n.t('No Movies in this Category');
        this.getWidget('assets').model.set('buttons', assets);
        this.getWidget('assets').setTitle(t);
        this.selectFirstAsset();
    },

    selectFolder: function (e) {
        var $folder = $(e.target);
        this.blur('.' + this.selectedClass);
        this.focus($folder);
    },

    selectFirstFolder: function () {
        this.click(this.$('.folder').first());
    },

    selectFirstAsset: function () {
        this.focus(this.$('.asset').first());
    },

    assetLeft: function() {
        this.onKey(null, 'LEFT');
    },

    assetRight: function() {
        this.onKey(null, 'RIGHT');
    }
});

export default ContentCarousel;