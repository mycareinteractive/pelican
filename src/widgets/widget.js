import BaseLayout from '../layouts/baselayout'

const Widget = BaseLayout.extend({

    layoutType: 'widget',

    modelEvents: {
        'change': 'modelChanged'
    },

    preRender: false,
    nonInteractive: false,

    noFocusEvent: false,    // if this is true no native focus event will be fired on <a> focus

    hidden: false,

    constructor: function (options) {
        this.model = this.model || new Backbone.Model();
        Widget.__super__.constructor.apply(this, arguments);
    },

    onKey: function (e, key) {
        if (this.hidden) {
            return false;
        }
        return false;
    },

    onAttach: function () {
        if (this.hidden) {
            this.hide();
        }
    },

    onModelChange: function () {

    },

    modelChanged: function () {
        this.log('model changed');
        this.onModelChange.apply(this);
        this.render();
    },

    hide: function () {
        this.hidden = true;
        this.$el.hide();
    },

    show: function () {
        this.hidden = false;
        this.$el.show();
    }
});


export default Widget;