import Widget from './widget';
import ButtonGroup from './buttongroup';

var template = require('../templates/dialog.hbs');

/**
 * Dialog is a widget you can throw into a screen that handles pop-up style modal dialog
 */
const Dialog = Widget.extend({
    className: 'dialog',
    template: template,

    orientation: 'vertical',
    hidden: 'hidden',

    widgets: {
        buttons: {
            widgetClass: ButtonGroup,
            selector: '#buttons'
        },
    },

    modelEvents: {
        'change:buttons': 'buttonsChanged',
        'change:content': 'contentChanged'
    },

    onRender: function () {
        this.getWidget('buttons').orientation = this.orientation;
    },

    onKey: function (e, key) {
        if (this.hidden) {
            return false;
        }

        var handled = false;
        if (key == 'UP' || key == 'DOWN' || key == 'LEFT' || key == 'RIGHT') {
            handled = true;
            this.getWidget('buttons').onKey(e, key);
        }

        else if (key == 'ENTER') {
            handled = true;
            let $active = this.$('.active').filter(':visible').first();
            if ($active.length > 0) {
                this.click($active);
            }
        }

        this.log('key:', key, ', handled:', handled);
        return handled;
    },

    buttonsChanged: function(model, data) {
        $.each(data, function(i, v){
            v.id = v.id || '#';
        });
        this.getWidget('buttons').model.set('buttons', data);
    },

    contentChanged: function(model, data) {
        this.$('#instruction').html(data);
    }
});

export default Dialog;