import Widget from './widget';

var template = require('../templates/dropdownmenu.hbs');

/**
 * DropdownMenu is a horizontal 2-level cascade menu that is used mainly on home page.
 */
const DropdownMenu = Widget.extend({
    className: 'dropdown-menu',
    template: template,

    // cached jQuery objects
    $menuGroupButtons: $(),
    $menuGroups: $(),
    $slides: $(),
    events: {
        'click .menu-group-button': 'selectGroup'
    },

    onRender: function () {
        this.$menuGroupButtons = this.$('.major');
        this.$menuGroups = this.$('.menu-group');
        this.$slides = this.$('.slide');
    },

    onKey: function (e, key) {
        var keyIndex = ['UP', 'DOWN', 'LEFT', 'RIGHT'].indexOf(key);
        if (keyIndex == -1)
            return false;

        var handled = false;

        if (keyIndex < 2) { // UP and DOWN
            handled = true;
            this.moveFocus(key == 'UP' ? 'prev' : 'next', this.$menuGroups.filter(':visible'));
        }
        else { // LEFT and RIGHT
            handled = true;
            var $nextGroup = this.moveFocus(key == 'LEFT' ? 'prev' : 'next', this.$menuGroupButtons, this.selector, 1, this.selectedClass, true);
            this.click($nextGroup); // this.click() uses DOM API instead of jQuery.click to speed up
        }

        this.log('key:', key, ', handled:', handled);
        return handled;
    },

    selectGroup: function (e) {
        var $obj = $(e.target);
        this.blur('.' + this.selectedClass);
        this.focus($obj, this.selectedClass, this.noFocusEvent);
        this.showGroup($obj);
    },

    showGroup: function ($obj) {
        if (!$obj || $obj.length < 1)
            return;
        this.blur('.' + this.activeClass);
        var groupId = '#' + $obj.attr('id') + '-group';
        var slideId = '#' + $obj.attr('id') + '-slide';
        this.$menuGroups.hide();
        this.$(groupId).show();
        this.$slides.hide();
        this.$(slideId).show();
        var $nextLink = this.$(groupId).find('a').first();
        this.focus($nextLink, this.activeClass, this.noFocusEvent);
    }
});

export default DropdownMenu;