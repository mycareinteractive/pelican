import Widget from './widget';

var template = require('../templates/buttongroup.hbs');

/**
 * ButtonGroup is a group of button that you can navigate between.
 * ButtonGroup supports both vertical (default) and horizontal layout.
 */
const ButtonGroup = Widget.extend({
    className: 'button-group',
    template: template,

    orientation: 'vertical',
    nonActive: false,

    onRender: function() {
        if(this.nonActive) {
            this.$el.addClass('nonactive');
        }
    },

    onKey: function (e, key) {
        var direction = '', handled = false;
        // orientation defaults to vertical
        if (this.orientation == 'horizontal') {
            if (key == 'LEFT')
                direction = 'prev';
            else if (key == 'RIGHT')
                direction = 'next';
        }
        else {
            if (key == 'UP')
                direction = 'prev';
            else if (key == 'DOWN')
                direction = 'next';
        }

        if (direction) {
            handled = true;
            this.moveFocus(direction, this.$el);
        }
        else {
            handled = false;
        }

        this.log('key:', key, ', handled:', handled);
        return handled;
    }
});

export default ButtonGroup;