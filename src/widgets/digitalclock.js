import Widget from './widget';
import '../helpers/datetime';
import '../helpers/dotimeout';
var template = require('../templates/digitalclock.hbs');

const DigitalClock = Widget.extend({
    // override properties
    className: 'digital-clock',
    template: template,
    nonInteractive: true,

    // self properties
    format: 'ddd mmm dd yyyy HH:MM',
    locale: 'en',
    dateTimeClass: 'datetime',
    timerId: '',

    onInit: function () {
        this.timerId = 'digital-clock.' + this.cid;
        var interval = 10000;
        if(this.format.indexOf('s') >= 0) { // if displaying seconds, we need to run every second
            interval = 1000;
        }
        $.doTimeout(this.timerId, interval, this.displayTime.bind(this));
    },

    onRender: function() {
        this.displayTime();
    },

    onBeforeDestroy: function () {
        $.doTimeout(this.timerId);
    },

    displayTime: function () {
        var str = this.generateTime();
        this.$('p').text(str);
        return true;
    },

    generateTime: function() {
        var d = new Date();
        return d.format(this.format, false, this.locale);
    }
});

export default DigitalClock;
