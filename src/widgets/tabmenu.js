import Widget from './widget';
var template = require('../templates/tabmenu.hbs');

const TabMenu = Widget.extend({
    // override properties
    className: 'tab-menu',
    template: template,

    // self properties
    backButton: true,

    // cached jQuery objects
    $menuTabButtons: $(),
    $menuTabs: $(),
    $menuTab: $(),
    $prevPageButton: $(),
    $nextPageButton: $(),

    events: {
        'focus .back-button': 'focusBackButton',
        'click .menu-tab-button': 'selectTabButton',
        'click .sub-page-prev': 'previousSubPage',
        'click .sub-page-next': 'nextSubPage'
    },

    templateHelpers: function () {
        return {
            backButton: this.backButton
        }
    },

    onRender: function () {
        this.$menuTabButtons = this.$('.major');
        this.$menuTabs = this.$('.menu-tab');
        this.$prevPageButton = this.$('.sub-page-prev');
        this.$nextPageButton = this.$('.sub-page-next');
    },

    onKey: function (e, key) {
        var keyIndex = ['UP', 'DOWN', 'LEFT', 'RIGHT'].indexOf(key);
        if (keyIndex == -1)
            return false;

        var handled = false;

        if (keyIndex < 2) { // UP and DOWN
            handled = true;
            var $nextButton = this.moveFocus(key == 'UP' ? 'prev' : 'next', this.$menuTabButtons);
            if ($nextButton.hasClass('menu-tab-button')) {
                this.showTab($nextButton);
            }
            else if ($nextButton.hasClass('back-button')) {
                this.showTab($nextButton);
            }
        }
        else { // LEFT and RIGHT
            handled = true;
            var $pages = this.$menuTab.find('.sub-page');
            var $page = $pages.filter(':visible');
            if ($page.find('a').length > 1) {
                // if sub-page has more than one button, LEFT/RIGHT will switch button instead of pages
                this.moveFocus(key == 'LEFT' ? 'prev' : 'next', $page, 'a');
            }
            else if ($pages.length > 1) {
                if (key == 'LEFT') {
                    this.click(this.$prevPageButton);
                }
                else if (key == 'RIGHT') {
                    this.click(this.$nextPageButton);
                }
            }
        }

        this.log('key:', key, ', handled:', handled);
        return handled;
    },

    focusBackButton: function (e) {
        this.showTab($(e.target));
    },

    selectTabButton: function (e) {
        var $tabBtn = $(e.target);
        this.blur('.' + this.selectedClass);
        this.focus($tabBtn);
        this.showTab($tabBtn);
    },

    showTab: function ($tabBtn) {
        if (!$tabBtn || $tabBtn.length < 1)
            return;

        this.blur('.' + this.activeClass);
        // show the tab
        this.$menuTabs.hide();
        var $tab = this.$menuTabs.filter('#' + $tabBtn.attr('id') + '-tab');
        this.$menuTab = $tab;
        this.log('show tab:', '#' + $tab.attr('id'));
        $tab.show();
        // show sub-pages inside tab
        var $pages = $tab.find('.sub-page');
        $pages.hide();
        $pages.first().show();
        this.focusButtonInTab($tab);

        // if there are multiple buttons within a single sub-page, we can't use LEFT/RIGHT to scroll sub-pages
        this.$('.arrow-button').hide();
        if ($pages.length > 1 && $pages.first().find('a').length < 2) {
            this.$('.arrow-button').show();
        }
    },

    previousSubPage: function (e) {
        var $tabs = this.$menuTab;
        var $pages = $tabs.find('.sub-page');
        var $page = $pages.filter(':visible').prev();
        if ($page.length < 1) {
            $page = $pages.last();
        }
        $pages.hide();
        $page.show();
    },

    nextSubPage: function (e) {
        var $tabs = this.$menuTab;
        var $pages = $tabs.find('.sub-page');
        var $page = $pages.filter(':visible').next();
        if ($page.length < 1) {
            $page = $pages.first();
        }
        $pages.hide();
        $page.show();
    },

    focusButtonInTab: function ($tab) {
        var $pages = $tab.find('.sub-page');
        if ($pages.length > 1) {
            this.focus($pages.first().find('a').first());
        }
        else {
            this.focus($tab.find('a').first());
        }

    }

});

export default TabMenu;
