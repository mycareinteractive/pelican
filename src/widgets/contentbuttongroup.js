import Widget from './widget';

var template = require('../templates/contentbuttongroup.hbs');

/**
 * ContentButtonGroup is an enhanced version of ButtonGroup widget that supports paging/scrolling.
 */
const ContentButtonGroup = Widget.extend({
    className: 'content-button-group',
    template: template,

    // self properties
    backButton: true,
    arrows: true,
    nonActive: false,
    orientation: 'vertical',
    contentClass: '',
    contentUrlPrefix: 'education',

    templateHelpers: function () {
        return {
            backButton: this.backButton,
            contentClass: this.contentClass,
            contentUrlPrefix: this.contentUrlPrefix
        }
    },

    onRender: function () {
        if (this.nonActive) {
            this.$el.addClass('nonactive');
        }

        if (this.orientation == 'horizontal') {
            this.$el.css('overflow-x', 'scroll');
        }
        else {
            this.$el.css('overflow-y', 'scroll');
        }

        this.updateArrows();
    },

    onAttach: function () {
        this.$el.parent().css('overflow', 'hidden');
    },

    onKey: function (e, key) {
        var direction = '', handled = false;

        // navigation keys
        if (['UP', 'DOWN', 'LEFT', 'RIGHT'].indexOf(key) >= 0) {
            // orientation defaults to vertical
            if (this.orientation == 'horizontal') {
                if (key == 'LEFT')
                    direction = 'prev';
                else if (key == 'RIGHT')
                    direction = 'next';
            }
            else {
                if (key == 'UP')
                    direction = 'prev';
                else if (key == 'DOWN')
                    direction = 'next';
            }

            if (direction) {
                handled = true;
                this.moveFocus(direction, this.$el);
            }
        }
        else if (key == 'PGUP' || key == 'PGDN' || key == 'CHUP' || key == 'CHDN') {
            handled = true;
            direction = (key == 'PGUP' || key == 'CHUP') ? 'prev' : 'next';
            this.scrollPage(direction, this.$el, 'a');
        }

        this.updateArrows();
        this.log('key:', key, ', handled:', handled);
        return handled;
    },

    selectFirst: function () {
        var $obj = this.$el.find('a').first();
        this.focus($obj);
    },

    updateArrows: function () {
        if(!this.arrows)
            return;

        if(this.$el && this.$el.length > 0) {
            if (this.orientation == 'vertical') { // vertical
                if (this.$el[0].scrollHeight <= this.$el.innerHeight()) {    // single page
                    this.$('.arrow').hide();
                }
                else {
                    this.$('.arrow').show();
                    if (this.$el[0].scrollHeight - this.$el.scrollTop() - this.$el.height() <= 15) // bottom
                    {
                        this.$('.arrow-next').hide();
                    }
                    else if(this.$el.scrollTop() <= 15) // top
                    {
                        this.$('.arrow-prev').hide();
                    }
                }
            }
            else { // horizontal
                if (this.$el[0].scrollWidth <= this.$el.innerWidth()) {    // single page
                    this.$('.arrow').hide();
                }
                else {
                    this.$('.arrow').show();
                    if (this.$el[0].scrollWidth - this.$el.scrollLeft() - this.$el.width() <= 15) // right most
                    {
                        this.$('.arrow-next').hide();
                    }
                    else if(this.$el.scrollLeft() <= 15) // left most
                    {
                        this.$('.arrow-prev').hide();
                    }
                }
            }

        }
    }

});

export default ContentButtonGroup;