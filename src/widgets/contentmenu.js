import Widget from './widget';
import ContentButtonGroup from './contentbuttongroup';

var template = require('../templates/contentmenu.hbs');

/**
 * ContentMenu is a two column widget for displaying categorized content.
 * ContentMenu listens to two model attribute changes: "folders" and "assets"
 */
const ContentMenu = Widget.extend({
    className: 'content-menu',
    template: template,

    // self properties

    // cached jQuery objects
    $folders: $(),
    $assets: $(),

    widgets: {
        folders: {
            widgetClass: ContentButtonGroup,
            selector: '.major',
            options: {backButton: true, nonActive: true, contentClass: 'folder'}
        },
        assets: {
            widgetClass: ContentButtonGroup,
            selector: '.minor',
            options: {backButton: false, nonActive: false, contentClass: 'asset'}
        }
    },

    modelEvents: {
        'change:folders': 'foldersChanged',
        'change:assets': 'assetsChanged'
    },

    events: {
        'click .folder': 'selectFolder'
    },

    templateHelpers: function () {
        return {}
    },

    onRender: function () {

    },

    onAttach: function () {
        this.$folders = this.$('.folder');
        this.$assets = this.$('.asset');
    },

    onKey: function (e, key) {
        var handled = false;

        if (key == 'RIGHT') {
            // shift focus to assets
            handled = true;
            if (this.$assets.length > 0 && this.$assets.filter('.active').length < 1) {
                this.selectFirstAsset();
            }
        }
        else if (key == 'LEFT') {
            // shift focus to folders
            handled = true;
            this.blur(this.$assets.filter('.active'));
        }
        else if (key == 'UP' || key == 'DOWN' || key == 'PGUP' || key == 'PGDN' || key == 'CHUP' || key == 'CHDN') {
            // nav inside folders or assets, let child widgets handle them
            handled = true;
            if (this.$assets.filter('.active').length > 0) {
                this.getWidget('assets').onKey(e, key);
            }
            else {
                this.getWidget('folders').onKey(e, key);
                // folders automatically gets a click if nav using remote
                var $nextButton = this.$folders.filter('.selected');
                if ($nextButton.hasClass('folder')) {
                    this.click($nextButton);
                }
            }
        }

        this.log('key:', key, ', handled:', handled);
        return handled;
    },

    foldersChanged: function (model, folders) {
        this.getWidget('folders').model.set('buttons', folders);
        this.getWidget('assets').model.clear();
        this.$folders = this.$('.folder');
    },

    assetsChanged: function (model, assets) {
        this.getWidget('assets').model.set('buttons', assets);
        this.$assets = this.$('.asset');
    },

    selectFolder: function (e) {
        var $folder = $(e.target);
        this.blur('.' + this.selectedClass);
        this.focus($folder);
    },

    selectFirstFolder: function () {
        this.click(this.$('.folder').first());
    },

    selectFirstAsset: function () {
        this.focus(this.$('.asset').first());
    }
});

export default ContentMenu;