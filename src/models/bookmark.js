import Backbone from 'backbone';
import Bookmarks from '../collections/bookmarkcollection';

const Bookmark = Backbone.Model.extend({
    /*url: function() {
        //console.log('MODEL ID: ' + this.get('slug'));
        // GET /api/v1/me/bookmarks/{id}
        return "/me/bookmarks/" + this.id;
    },*/

    urlRoot: "/me/bookmarks",

    initialize: function(attributes, options) {
        this.patch(attributes);
    },

    // backbone will automatically call this function
    parse: function(data, options) {
        this.patch(data);
        return data;
    },

    patch(data) {
        var self = this;
        if (!data) {
            return;
        }
        if ($.isArray(data)) {
            // if it's an array just patch each item in array
            $.each(data, function (index, child) {
                self.patch(child);
            });
        }
        else if (data) {
            this.addBookmarkId(data);
            this.addContentTypeFlags(data);
            this.addClassName(data);
            this.addTitle(data.content);
        }
    },

    addContentTypeFlags: function(node) {
        node.isFolder = (node.contentType === "Folder");
        node.isAsset =
            (node.contentType === "Image"
            || node.contentType === "Video"
            || node.contentType === "Audio"
            || node.contentType === "Document");
    },

    addTitle: function(content) {
        if(!content)
            return;
        content.metadatas = content.metadatas || {};
        content.metadatas['title'] = content.metadatas['title'] || content.name;
    },

    addBookmarkId: function(node) {
        node.bookmarkId = node.id;
    },

    addClassName: function(node) {
        node.className = '';

        if(node.status == 'Completed') {
            node.className += ' completed';
        }

        if(node.orderID) {
            node.className += ' prescribed';
        }
    }

});

export default Bookmark;
