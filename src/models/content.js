import Backbone from 'backbone';
import Contents from '../collections/contentcollection';

const Content = Backbone.Model.extend({
    url: function() {
        //console.log('MODEL ID: ' + this.get('slug'));
        return this.uri || "/contents/slug";
    },

    htmlDecode: function(encoded){
        var hcont = $('<textarea />').html(encoded).val();
        return hcont;
    },

    initialize: function(attributes, options) {
        this.patch(attributes);

        if (this.get('children')) {
            this.set({
                items: new Contents(this.get('children'))
            });

            this.get('items').each(function(i){
                console.log('THE CHILDREN\'S NAME: ' + i.get('name'));
            });
        }
        
    },

    // backbone will automatically call this function
    parse: function(data, options) {
        this.patch(data);
        return data;
    },

    patch(data) {
        var self = this;
        if (!data) {
            return;
        }
        if ($.isArray(data)) {
            // if it's an array just patch each item in array
            $.each(data, function (index, child) {
                self.patch(child);
            });
        }
        else if (data) {
            this.addContentTypeFlags(data);
            this.addPath(data);
            this.addTitle(data.content);
            this.decodeHtmlMetadatas(data.content);

            // patch children recursively
            self.patch(data.children);
        }
    },

    addPath: function(node) {
        var slug = node.slug;
        var path = slug;

        var prefix = App.config.baseSlug;

        // replace baseSlug part with 'home/'
        if (prefix && slug && slug.startsWith(prefix)) {
            path = slug.replace(prefix, 'home/');
        }

        // action will use metadata 'action'
        if (node.contentType == "Action") {
            if (node.content && node.content.metadatas && node.content.metadatas['action']) {
                path = node.content.metadatas['action'];
            }
        }

        node.path = path;
    },

    addContentTypeFlags: function(node) {
        node.isFolder = (node.contentType === "Folder");
        node.isAction = (node.contentType === "Action");
        node.isHtml = (node.contentType === "Html");
        node.isAsset =
            (node.contentType === "Image"
            || node.contentType === "Video"
            || node.contentType === "Audio"
            || node.contentType === "Document");
    },

    addTitle: function(content) {
        if(!content)
            return;
        content.metadatas = content.metadatas || {};
        content.metadatas['title'] = content.metadatas['title'] || content.name;
    },

    decodeHtmlMetadatas: function(content){
        if(!content)
            return;
        var md = content.metadatas || {};
        md.htmlContent = this.htmlDecode(md.htmlContent);
        md.htmlText1 = this.htmlDecode(md.htmlText1);
        md.htmlText2 = this.htmlDecode(md.htmlText2);
        md.htmlImage1 = this.htmlDecode(md.htmlImage1);
        md.htmlImage2 = this.htmlDecode(md.htmlImage2);
    },
});

export default Content;
