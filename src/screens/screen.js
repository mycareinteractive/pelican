import Radio from 'backbone.radio';
import {KeyCodes} from 'pelican-device';
import BaseLayout from '../layouts/baselayout';

function trueFn() {
    return true;
}
function falseFn() {
    return false;
}

const Screen = BaseLayout.extend({

        layoutType: 'screen',

        keepPreviousScreen: false,

        keyEvents: {},

        hidden: true,

        noPageView: false,

        // Marionette/Backbone methods
        initialize: function () {
            BaseLayout.prototype.initialize.apply(this, arguments);
        },

        // Override Marionette delegateEvents to handle keyEvents and widgetEvents
        delegateEvents: function (events) {
            Marionette.LayoutView.prototype.delegateEvents.apply(this, arguments);
            this.bindKeyEvents();
            this.bindWidgetEvents();
        },

        undelegateEvents: function () {
            Marionette.LayoutView.prototype.undelegateEvents.apply(this, arguments);
            this.unbindKeyEvents();
            this.unbindWidgetEvents();
        },

        // public methods

        /**
         * Show screen.  This is used when navigating back to previous screen which is already in DOM but hidden.
         */
        show: function (options) {
            this.onBeforeScreenShow(options);
            this.undelegateEvents();
            this.delegateEvents();
            this.$el.show();
            this.hidden = false;
            this.onScreenShow(options);
        },

        /**
         * Hide screen and detach all the listeners
         */
        hide: function (options) {
            this.onBeforeScreenHide(options);
            this.undelegateEvents();
            if (options && options.visible) {
                // keep if screen visible but unbind all events, useful for being under overlay/popup screens
            }
            else {
                this.$el.hide();
            }
            this.hidden = true;
            this.onScreenHide(options);
        },

        /**
         * Check if screen is hidden
         * @returns {boolean}
         */
        isHidden: function () {
            return this.hidden;
        },

        /**
         * Remove screen from DOM
         */
        removeScreen: function() {
            this.onBeforeScreenRemove();
            this.$el.remove();
            this.onScreenRemove();
        },

        /**
         * Close current screen and go back
         */
        close: function () {
            this.triggerMethod('close:screen');
        },

        back: function () {
            this.triggerMethod('close:screen', true);
        },

        home: function () {
            this.triggerMethod('show:bottomScreen', true);
        },

        // Listen to key events
        bindKeyEvents: function () {
            var channel = Radio.channel('device');
            channel.on('key', this.keyHandler, this);
        },

        // Remove key listeners
        unbindKeyEvents: function () {
            var channel = Radio.channel('device');
            channel.off('key', this.keyHandler, this);
        },

        bindWidgetEvents: function () {
        },

        unbindWidgetEvents: function () {
        },

        // protected methods for screens to override

        /**
         * This will be called after a screen is initialized.
         * @param options
         */
        onInit: function (options) {
        },

        /**
         * Gets called before screen is shown.  Eg: screen first show, or screen re-show after above screens destroyed
         */
        onBeforeScreenShow: function () {
        },

        /**
         * Gets called after screen is shown.  Eg: screen first show, or screen re-show after above screens destroyed
         */
        onScreenShow: function () {
        },

        /**
         * Gets called before screen is hidden.
         */
        onBeforeScreenHide: function () {
        },

        /**
         * Gets called after screen is hidden.
         */
        onScreenHide: function (options) {
        },

        /**
         * Gets called before screen is removed.
         * @param options
         */
        onBeforeScreenRemove: function (options) {
        },

        /**
         * Gets called after screen is removed.
         * @param options
         */
        onScreenRemove: function (options) {
        },

        // private methods

        keyHandler: function (e, key) {
            var handled = false;

            // send key to widgets first
            handled = this.onKey(e, key);

            // process by the screen event handlers
            var keyEvents = this.keyEvents;
            if ($.isFunction(this.keyEvents))
                keyEvents = this.keyEvents();

            keyEvents = keyEvents || {};

            var method = keyEvents[key];
            if (method === true) method = trueFn;
            else if (method === false) method = falseFn;
            else if (!$.isFunction(method)) method = this[method];
            if (method) {
                handled = method.call(this, e, key);
                if (typeof handled === 'undefined') // if key event does not return value, assume handled
                    handled = true;
            }

            if (!e['enableDefault']) {  // Sometimes we want browser to handle keys. eg: inside input elements
                e.preventDefault();
            }

            if (!handled) {
                // Special key logic
                if (key == KeyCodes.Close || key == KeyCodes.Back || key == KeyCodes.Exit) {   // go back one screen
                    handled = true;
                    this.back();
                }
                else if (key == KeyCodes.Menu || key == KeyCodes.Home) {   // go to home page
                    handled = true;
                    this.home();
                }
                else if (key == KeyCodes.Enter) {
                    let $active = this.$('.active').filter(':visible').first();
                    if ($active.length < 1)
                        $active = this.$('.selected').filter(':visible').first();

                    // JQuery .click() will not actually trigger DOM clicking.  Use HTMLElement.click()
                    if ($active.length > 0) {
                        handled = true;
                        $active[0].click();
                    }
                }
            }

            // Power key is special, we return false regardless of handled so firmware can act on power
            if (key == KeyCodes.Power) {
                handled = false;
                this.home();
            }

            e.handled = handled;
            this.log('key:' + key + ', handled: ' + handled);
        }
    })
    ;

export default Screen;