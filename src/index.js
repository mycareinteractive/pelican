var Pelican = {};

// common includes
require('./helpers/extensions');
require('./helpers/handlebars-helpers');
require('./helpers/backbone.sync.override');

// device
Pelican.Desktop = require('./device/desktop').default;

// modules
Pelican.Application = require('./common/application').default;
Pelican.Router = require('./common/router').default;
Pelican.Screen = require('./screens/screen').default;
Pelican.StackLayout = require('./layouts/stacklayout').default;
Pelican.Tracker = require('./common/tracker').default;

// widgets
Pelican.Widget = require('./widgets/widget').default;
Pelican.ButtonGroup = require('./widgets/buttongroup').default;
Pelican.DropdownMenu = require('./widgets/dropdownmenu').default;
Pelican.TabMenu = require('./widgets/tabmenu').default;
Pelican.DigitalClock = require('./widgets/digitalclock').default;
Pelican.ContentButtonGroup = require('./widgets/contentbuttongroup').default;
Pelican.ContentPosterGroup = require('./widgets/contentpostergroup').default;
Pelican.ContentMenu = require('./widgets/contentmenu').default;
Pelican.ContentCarousel = require('./widgets/contentcarousel').default;
Pelican.Dialog = require('./widgets/dialog').default;

// models
Pelican.Collections = {};
Pelican.Collections.ContentCollection = require('./collections/contentcollection').default;
Pelican.Collections.BookmarkCollection = require('./collections/bookmarkcollection').default;

Pelican.Models = {};
Pelican.Models.Content = require('./models/content').default;
Pelican.Models.Bookmark = require('./models/bookmark').default;

// utilities
Pelican.Browser = require('./common/browser').default;

export default Pelican;