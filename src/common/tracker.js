export default class Tracker {

    constructor() {
        this.provider = '';
        this.providerUrl = '';
        this.siteId = '';
        this.clientId = undefined;
        this.userId = undefined;
        this.lastPage = undefined;
    }

    init(provider, providerUrl, siteId, clientId) {
        var self = this;
        self.provider = provider;
        self.providerUrl = providerUrl;
        self.siteId = siteId;
        self.clientId = clientId;

        switch (self.provider) {
            case 'google':
                // load GA lib
                window._ga = undefined;
                (function (i, s, o, g, r, a, m) {
                    i['GoogleAnalyticsObject'] = r;
                    i[r] = i[r] || function () {
                            (i[r].q = i[r].q || []).push(arguments)
                        }, i[r].l = 1 * new Date();
                    a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                    a.async = 1;
                    a.src = g;
                    m.parentNode.insertBefore(a, m)
                })(window, document, 'script', '//www.google-analytics.com/analytics.js', '_ga');

                _ga('create', self.siteId, {
                    'cookieDomain': 'none',
                    'storage': 'none',
                    'clientId': self.clientId
                });
                _ga('set', 'hostname', window.location.host);
                _ga('set', 'dimension1', self.clientId);

                break;

            case 'piwik':
                // load piwik lib
                window._paq = window._paq || [];
                (function () {
                    var u = self.providerUrl;
                    _paq.push(["setTrackerUrl", u + "piwik.php"]);
                    _paq.push(["setSiteId", this.siteId]);
                    _paq.push(['setSessionCookieTimeout', 1800]);
                    _paq.push(['disableCookies']);
                    var d = document, g = d.createElement("script"), s = d.getElementsByTagName("script")[0];
                    g.type = "text/javascript";
                    g.defer = true;
                    g.async = true;
                    g.src = u + "piwik.js";
                    s.parentNode.insertBefore(g, s);
                })();

                break;
        }
    }

    setUserId(userId) {
        if (userId)
            _ga('set', 'userId', userId);
    }

    pageView(url, title, options) {
        if (!url.startsWith('/')) {
            url = '/' + url;
        }
        this.lastPage = url;

        if(title) {
            document.title = title;
        }

        if (this.provider == 'google') {
            options = options || {};
            options['hitType'] = 'pageview';
            options['page'] = url;
            if (title) {
                options['title'] = title;
            }
            _ga('send', options);
        }
        else if (this.provider == 'piwik') {
            if (url)
                _paq.push(['setCustomUrl', url]);
            if (title)
                _paq.push(['setDocumentTitle', title]);
            _paq.push(['trackPageView']);
        }
    }

    customDimension(index, name, value, scope) {
        if (this.provider == 'google') {
            _ga('set', 'dimension' + index, value);
        }
        else if (this.provider == 'piwik') {
            _paq.push(['setCustomVariable', index, name, value, scope]);
        }
    }

    event(category, action, label, value, options) {
        if (this.provider == 'google') {
            options = options || {};
            options['hitType'] = 'event';
            if (typeof options.nonInteraction === 'undefined')
                options['nonInteraction'] = false;
            if (typeof options.page === 'undefined' && this.lastPage)
                options['page'] = this.lastPage;

            options['eventCategory'] = category;
            options['eventAction'] = action;
            if (label)
                options['eventLabel'] = label;
            if (value)
                options['eventValue'] = value;
            _ga('send', options);
        }
        else if (this.provider == 'piwik') {
            _paq.push(['trackEvent', category, action, label, value]);
        }
    }

    heartbeat() {
        this.event('heartbeat', 'heartbeat');
    }
}