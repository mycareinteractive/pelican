export default class Browser {

    static supportCustomEvent() {
        return ('CustomEvent' in window &&
            // in Safari, typeof CustomEvent == 'object' but it works
            (typeof window.CustomEvent === 'function' ||
                (window.CustomEvent.toString().indexOf('CustomEventConstructor') > -1))
        );
    }

    static supportHashChange() {
        return ('onhashchange' in window &&
            (document.documentMode === void 0 || document.documentMode > 7));
    }
}