const Router = Marionette.AppRouter.extend({

    constructor: function () {
        // create a dummy controller that has all routes as a noop function
        var controller = {};
        $.each(this.appRoutes, function (key, val) {
            controller[val] = $.noop;
        });
        this.controller = controller;

        // add notFound route
        this.controller['notFound'] = $.noop;
        this.appRoute('*notFound', 'notFound');

        Marionette.AppRouter.prototype.constructor.apply(this, arguments);
    },

    initialize: function () {
        this.bindMouseEvents();
    },

    execute: function (callback, args, name) {
        console.log('--------------------- before route ---------------------');
        console.log('( Router ) execute name:' + name);
        if (!name || name === 'notFound') {
            console.log('route not found, route cancelled');
            App.layout.showTopScreen();
            return false;
        }
        var ScreenClass = this.getScreen(name);
        if (!ScreenClass) {
            console.log('screen class not found, route cancelled');
            App.layout.showTopScreen();
            return false;
        }
        return true;
    },

    // this function will be called right after router started
    onStart: function () {
    },

    // Be aware the `routePath` here might be incorrect.  See https://github.com/marionettejs/backbone.marionette/issues/2992
    onRoute: function (name, routePath, params) {
        var path = this.getFullPath();
        console.log('--------------------- route ---------------------');
        console.log('( Router ) route name:' + name + ', path:' + path + ', params:' + params);
        if (!name)
            return;
        var id = this.getId();
        var path = this.getPath();
        var queries = this.getQueries();
        var screen = App.layout.findScreen(id);
        if (!screen) {
            var ScreenClass = this.getScreen(name);
            if (!ScreenClass) {
                return;
            }
            screen = new ScreenClass({
                id: id,
                path: path,
                params: params,
                queries: queries
            });
        }
        if(!screen.noPageView)
            App.tracker.pageView(path, id);
        App.layout.openScreen(screen);
    },

    getScreen: function (name) {
        return undefined;
    },

    getFullPath: function () {
        var fragment = Backbone.history.getFragment();
        return fragment;
    },

    getId: function () {
        var path = this.getPath();
        var id = path;
        return id.replace(/[\.:#/]/g, '-');
    },

    getPath: function () {
        var path = this.getFullPath();
        path = path.split('?')[0];
        return path;
    },

    getQueries: function () {
        var queries = {};
        var path = this.getFullPath();
        var queryString = path.split('?')[1];
        if (!queryString)
            return queries;

        var vars = queryString.split('&');
        for (var i = 0; i < vars.length; i++) {
            var pair = vars[i].split('=');
            var key = decodeURIComponent(pair[0]);
            if (key) {
                queries[key] = decodeURIComponent(pair[1]);
            }
        }
        return queries;
    },

    bindMouseEvents: function () {
        $(document).on('click', 'a,input,button', function (e) {
            e.preventDefault();
            var link = $(e.currentTarget).prop('hash');
            if (!link || link == '#' || !link.startsWith('#') || link.endsWith('/#')) {
                return false;
            }

            link = link.substr(1);
            if (!e.isPropagationStopped() && !e.isImmediatePropagationStopped()) {
                return this.go(link);
            }
            // TODO: how to detect invalid link click and prevent it from going to notfound route.
            // TODO: how to handle back logic.
        }.bind(this));
    },

    go: function (fragment, options) {
        var opts = {trigger: true, replace: true};
        $.extend(opts, this.options);
        return this.navigate(fragment, opts);
    }

});

export default Router;