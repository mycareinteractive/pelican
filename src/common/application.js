import Browser from './browser';

// a global App object/namespace
const Application = Marionette.Application.extend({

    layout: {},
    router: {},
    config: {},

    onBeforeStart: function () {
        console.log('=================== App before start ===================');
    },

    onStart: function () {
        console.log('=================== App start ===================');
        if (Backbone.history) {
            Backbone.history.start();
        }

        if (this.router && this.router.onStart) {
            this.router.onStart();
        }

        $('#loading').hide();
    },

    onDeviceReady: function () {
        console.log('=================== Device ready ===================');
        this.loadConfig();
    },

    onConfigReady: function() {
        console.log('=================== Config ready ===================');
        this.start();
    },

    initialize: function (options) {
        $.extend(this, this.options);
        $.ajaxSetup({
            timeout: 20000 //global timeout is 20 seconds
        });
    },

    progressMessage: function (msg) {
        $('<li/>').text(msg).appendTo('body>#loading>#loading-progress');
    },

    loadConfig: function () {
        var self = this;
        if(Browser.supportCustomEvent()) {
            document.addEventListener('configready', this.onConfigReady.bind(this), false);
        }
        else {
            $(document).on('configready', this.onConfigReady.bind(this));
        }
        // load production config first
        $.getJSON('./config.prod.json')
            .done(function (data) {
                console.log('Configuration loaded successfully from "config.prod.json"');
                self.config = data;
            })
            .fail(function () {
                console.log('Failed to load configuration from "config.prod.json"!');
            })
            .always(function () {
                // always try to load override config
                $.getJSON('./config.json')
                    .done(function (data) {
                        console.log('Override configuration loaded successfully from "config.json"');
                        $.extend(self.config, data);
                    })
                    .always(function () {
                        if(Browser.supportCustomEvent()) {
                            var e = new Event('configready');
                            document.dispatchEvent(e);
                        }
                        else {
                            $(document).trigger('configready');
                        }
                    });
            });

    },

    bootstrapDevice: function () {
        if(Browser.supportCustomEvent()) {
            document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
        }
        else {
            $(document).on('deviceready', this.onDeviceReady.bind(this));
        }
        if (window.PelicanDevice) {
            console.log('=================== Device bootstrap ===================');
            window.PelicanDevice.bootstrap(this.progressMessage);
        }
        else {
            console.error('Something is not right. Make sure you loaded correct Pelican platform package.');
        }
    }

});

export default Application;