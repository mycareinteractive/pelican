import Backbone from 'backbone';
import Content from '../models/content';

const Contents = Backbone.Collection.extend({
    url: '/contents/slug/descendants',
    model: Content,

    getMenu: function(){}
});

export default Contents;