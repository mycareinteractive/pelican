import Backbone from 'backbone';
import Bookmark from '../models/bookmark';

const Bookmarks = Backbone.Collection.extend({
    url: '/me/bookmarks',
    model: Bookmark,

    getMenu: function(){}
});

export default Bookmarks;