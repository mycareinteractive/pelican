import Device from 'pelican-device';
import {KeyCodes} from 'pelican-device';

export default class Desktop extends Device {
    constructor() {
        super();

        this.jwUrl = 'http://content.jwplatform.com/libraries/72SvWyVc.js';
        this.input = 'TV';
    }

    bootstrap() {
        var self = this;
        document.addEventListener('keydown', this._keyHandler.bind(this), false);

        // Opera 10.7 doesn't work well with jQuery().ready
        window.onload = function () {
            // add a JWPlayer for desktop
            $('<div id="desktop-video-layer"><div id="desktop-player"></div></div>').insertBefore('#app').css({
                "position": "fixed",
                "display": "block",
                "background": "transparent",
                "width": "1280px",
                "height": "720px"
            });

            // getting jw player lib
            $.getScript(self.jwUrl).done(function () {
                console.log('JWPlayer library loaded for desktop');
            }).fail(function () {
                console.log('Desktop error! Fail to load JWPlayer library!');
            });

            self.triggerDeviceReady();
        };
    }

    getPlatform() {
        return "DESKTOP";
    }

    getHardwareModel() {
        return "PC";
    }

    getHardwareVersion() {
        return "0.0";
    }

    getHardwareSerial() {
        return "1234567";
    }

    getMiddlewareVersion() {
        return "0.0.0";
    }


    getNetworkDevices() {
        return [{
            networkMode: 1,
            name: 'fake-eth0',
            ip: '127.0.0.1',
            mac: '0021F8034F72',
            gateway: '',
            netmask: ''
        }];
    }

    reload() {
        window.location.hash = '';
        window.location.reload();
    }

    reboot() {
        window.location.hash = '';
        window.location.reload();
    }

    playChannel(param) {
        var defer = jQuery.Deferred();

        return defer.promise();
    }

    stopChannel() {
        var defer = jQuery.Deferred();

        return defer.promise();
    }

    playMedia(param) {
        var self = this;
        var defer = jQuery.Deferred();

        jwplayer('desktop-player').setup({
            'file': param.url,
            'width': '100%'
        });

        jwplayer().once('ready', function () {
            jwplayer().setControls(false);
            jwplayer().play(true);
            defer.resolve();
        });

        jwplayer().on('complete', function () {
            console.log('jwplayer complete');
            var e = new CustomEvent('media_event_received');
            e.eventType = 'play_end';
            document.dispatchEvent(e);
        });

        jwplayer().on('firstFrame', function () {
            console.log('jwplayer complete');
            var e = new CustomEvent('media_event_received');
            e.eventType = 'play_start';
            document.dispatchEvent(e);
        });

        return defer.promise();
    }

    stopMedia() {
        var self = this;
        var defer = jQuery.Deferred();

        try {
            jwplayer().stop();
            jwplayer().remove();
        }
        catch (e) {
        }
        ;

        defer.resolve();
        return defer.promise();
    }

    pauseMedia() {
        var defer = jQuery.Deferred();
        var self = this;

        jwplayer().pause(true);

        defer.resolve();
        return defer.promise();
    }

    resumeMedia() {
        var defer = jQuery.Deferred();
        var self = this;

        jwplayer().pause(false);

        defer.resolve();
        return defer.promise();
    }

    setMediaPosition(positionInMs) {
        var self = this;
        var defer = jQuery.Deferred();

        try {
            jwplayer().seek(positionInMs / 1000);
            defer.resolve();
        }
        catch (e) {
            defer.reject();
        }

        return defer.promise();
    }

    getMediaPosition() {
        var self = this;
        var defer = jQuery.Deferred();

        try {
            var pos = jwplayer().getPosition();
            defer.resolve(pos * 1000);
        }
        catch (e) {
            defer.reject();
        }

        return defer.promise();
    }

    getMediaDuration() {
        var self = this;
        var defer = jQuery.Deferred();

        var duration = jwplayer().getDuration();

        defer.resolve(duration * 1000);
        return defer.promise();
    }

    setVideoLayerSize(param) {

        $('#desktop-video-layer').css({
            "x": param.x,
            "y": param.y,
            "width": param.width,
            "height": param.height
        });

        jwplayer().resize(param.width, param.height);
    }

    getVideoLayerSize() {
        var layer = $('#desktop-video-layer');
        var offset = layer.offset();
        return {
            x: offset.left,
            y: offset.top,
            width: layer.width(),
            height: layer.height()
        };
    }

    setExternalInput(input) {
        var defer = jQuery.Deferred();
        this.input = input;
        defer.resolve();
        return defer.promise();
    }

    getExternalInput() {
        var defer = jQuery.Deferred();
        defer.resolve(this.input);
        return defer.promise();
    }

    // private methods

    _keyHandler(e) {
        console.log('DESKTOP keydown, code ' + e.which);
        var key = keyMap[e.which];
        if (key) {
            this.triggerKey(e, key);
            console.log('Desktop browser key "' + key + '" handled: ' + e.handled);
        }
        else {
            console.log('Desktop browser unrecognized key ' + e.which);
        }
    }
}

const keyMap = {
    8: KeyCodes.Back,
    13: KeyCodes.Enter,
    19: KeyCodes.Pause,         // Pause/Break
    27: KeyCodes.Escape,        // Esc
    33: KeyCodes.PageUp,
    34: KeyCodes.PageDown,
    37: KeyCodes.Left,
    38: KeyCodes.Up,
    39: KeyCodes.Right,
    40: KeyCodes.Down,
    48: KeyCodes.Num0,
    49: KeyCodes.Num1,
    50: KeyCodes.Num2,
    51: KeyCodes.Num3,
    52: KeyCodes.Num4,
    53: KeyCodes.Num5,
    54: KeyCodes.Num6,
    55: KeyCodes.Num7,
    56: KeyCodes.Num8,
    57: KeyCodes.Num9,
    112: KeyCodes.Menu,         // F1
    113: KeyCodes.Exit,         // F2
    118: KeyCodes.ChDown,       // F7
    119: KeyCodes.ChUp,         // F8
    120: KeyCodes.Rewind,       // F9
    121: KeyCodes.Play,         // F10
    122: KeyCodes.Forward,      // F11
    123: KeyCodes.Stop,         // F12
    173: KeyCodes.Mute,         // mute on laptop FN keyboard
    174: KeyCodes.VolumeDown,   // vol- on laptop FN keyboard
    175: KeyCodes.VolumeUp,     // vol+ on laptop FN keyboard
    216: KeyCodes.Menu
};
