import App from '../common/application';
import upserver from 'upserver-client';

Backbone.sync = function(method, model, options){

    // DO NOT INSTANTIATE APP - this will re-initialize the app
    //var app = new App();
    var _url = window.App.config.server;
    var options = options || {};

    var methodMap = {
        'create': 'POST',
        'update': 'PUT',
        'patch': 'PATCH',
        'delete': 'DELETE',
        'read': 'GET'
    };

    // var type = methodMap[method];
    var type = methodMap[method];

    // Default JSON-request options.
    var params = {type: type, dataType: 'json'};

    // Ensure that we have a URL.
    if (!options.url) {
        params.url = _url + model.url || urlError();
    } else {
        params.url = _url + options.url;
    }

    params.url = (typeof model.url == 'function') ? model.url() : model.url;

    // Ensure that we have the appropriate request data.
    if (options.data == null && model && (method === 'create' || method === 'update' || method === 'patch')) {
        params.contentType = 'application/json';
        params.data = JSON.stringify(options.attrs || model.toJSON(options));
    }

    // For older servers, emulate JSON by encoding the request into an HTML-form.
    if (options.emulateJSON) {
        params.contentType = 'application/x-www-form-urlencoded';
        params.data = params.data ? {model: params.data} : {};
    }

    // Don't process data on a non-GET request.
    if (params.type !== 'GET' && !options.emulateJSON) {
        params.processData = false;
    }

    var pdata = (params.data) ? JSON.parse(params.data) : {};

    return upserver.api(params.url, type, $.extend({}, pdata, options.data))
        .done(function(data){
            console.log('SETTING MODEL DATA in Backbone.sync()');

            // Apply the backbone pipelines like parse()
            if(options.success) {
                options.success(data);
            }

            model.set(data);
        });

};
