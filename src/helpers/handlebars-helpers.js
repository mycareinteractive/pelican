var handlebarsRuntime = require("handlebars/runtime");
var i18n = require('i18next');

handlebarsRuntime.registerHelper('i18n', function(i18n_key) {
    var result = i18n.t(i18n_key);
    return new handlebarsRuntime.default.SafeString(result);
});
