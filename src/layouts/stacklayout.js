var StackLayout = Marionette.LayoutView.extend({

    // Define options for transitioning screens in and out
    defaults: {
        'class': 'stacklayout',
        itemClass: 'screen'
    },

    childEvents: {
        'close:screen': 'closeScreen',
        'show:bottomScreen': 'showBottomScreen'
    },

    initialize: function (options) {
        this.screens = [];
        options = options || {};
        this.options = this.defaults;
        $.extend(this.options, options);
    },

    render: function (container) {
        container = container || '#app';
        this.$el.addClass(this.options['class']);
        $(container).append(this.$el);
        return this;
    },

    // Close a screen
    closeScreen: function (screen, showScreenBelow, noBottomScreenProtection) {
        if (!screen || this.screens.length < 1)
            return;

        var idx = this.screens.indexOf(screen);

        if (idx <= 0 && !noBottomScreenProtection) {
            console.log('Can not close the bottom screen!  You should have at least one screen.');
            console.log('If you plan to open new page after closing, try call App.router.go("your new router") before calling Screen.close()');
            return;
        }

        // explicitly hide the screen, force all delegate being removed
        if (screen.isHidden()) {
            console.log('Screen is already hidden ' + screen.id);
        }
        else {
            console.log('Hiding screen ' + screen.id);
            screen.hide({});
        }

        // remove screen from list
        this.screens.splice(idx, 1);
        screen.removeScreen();

        // remove region and screen
        var region = this.getRegion(screen.id);
        if(region) {
            this.removeRegion(screen.id);
            region.$el.remove();
        }

        if (showScreenBelow && idx > 0) {
            var below = this.screens[idx - 1];
            this.showScreen(below);
        }
    },

    // Close the screens above current one
    closeAboveScreens: function (screen) {
        var self = this;
        var idx = this.screens.indexOf(screen);
        if (idx >= 0) {
            var aboveScreens = this.screens.slice(idx + 1);
            $.each(aboveScreens, function (i, s) {
                self.closeScreen(s);
            });
        }
    },

    // Close all screens
    closeAllScreens: function () {
        var self = this;
        var screens = this.screens.slice(0);
        $.each(screens, function (i, s) {
            self.closeScreen(s, false, true);
        });
    },

    // Push a new view onto the stack.
    // The itemClass will be auto-added to the parent element.
    openScreen: function (screen) {
        if (!screen || !screen.id) {
            console.log('Invalid screen to open: ' + screen);
            return;
        }

        // . : and # are not DOM id friendly
        var regionId = screen.id.replace(/[\.:#]/g, '-') + '-region';

        var existing = this.findScreen(screen.id);
        if (existing) {  // screen already exists
            this.closeAboveScreens(screen);
            screen.show({reshow: true});
        }
        else {
            // add region
            var $region = $('<div class="region screen-region"></div>').attr('id', regionId).css({
                "position": "fixed"
            });
            this.$el.append($region);
            this.addRegion(screen.id, '#' + regionId);
            // add screen to list
            this.screens.push(screen);
            // show screen
            screen.$el.addClass(this.options.itemClass);
            this.showChildView(screen.id, screen);
            screen.show({});
            // hide old screen
            var old = this.screens[this.screens.length - 2];
            if (old) {
                var o = {};
                if(screen.overlay) {
                    o.visible = true;
                }
                old.hide(o);
            }
        }

        // if screen is overlay-screen, show the underneath screen's $el but not wiring up events
        if (screen.$el.hasClass('overlay-screen')) {
            var old = this.screens[this.screens.length - 2];
            if (old && old.$el) {
                console.log('Hiding screen ' + old.id);
                old.$el.show();
            }
        }
    },

    // showScreen is different from openScreen.
    // showScreen will trigger router to change URL and then router triggers openScreen.
    showScreen: function (screen) {
        if (screen) {
            App.router.go(screen.path);
        }
    },

    showBottomScreen: function () {
        this.showScreen(this.screens[0]);
    },

    showTopScreen: function () {
        this.showScreen(this.screens[this.screens.length - 1]);
    },

    // Look for a screen
    findScreen: function (id) {
        if (id) {
            return this.screens.find(function (screen) {
                return screen.id == id;
            });
        }
    }

});

export default StackLayout;