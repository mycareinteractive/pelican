// All Pelican layouts, widgets and screens should inherit from here.
var BaseLayout = Marionette.LayoutView.extend({

    layoutType: '',

    selector: 'a',
    activeClass: 'active',
    selectedClass: 'selected',

    widgets: {},
    _widgets: null,

    initialize: function (options) {
        $.extend(this, this.options);
        Marionette.LayoutView.prototype.initialize.apply(this, arguments);
        try {
            this.initWidgets();
            this.onInit(options);
        }
        catch (e){
            console.log(e);
            if(e.stack)
                console.log(e.stack);
        }
    },

    onBeforeShow: function () {
        var _widgets = this.getWidgets();
        for (var key in _widgets) {
            let widget = _widgets[key];
            if (!widget)
                return;

            var selector = this.widgets[key].selector;
            var region = this.getRegion(key);

            this.$(region.el).addClass('region');
            widget.$el.addClass('widget').addClass(widget.className);

            if (widget.preRender) {
                // widget already rendered widget HTML, just set the `el`.
                region.attachView(widget);
                widget.setElement(this.$(region.el).find(' div:first-child'));
            }
            else {
                region.show(widget);
            }
        }
    },

    onDestroy: function(){
        for(var name in this._widgets) {
            delete this._widgets[name];
        }
        this._widgets = {};
    },

    /**
     * Override this method to implement your own key handling.
     * Default implementation is to send the key to every widget until one of them processes it.
     * @param e original key event from device.
     * @param key decoded key name.  Eg: "UP".
     */
    onKey: function (e, key) {
        var handled = false;
        var _widgets = this.getWidgets();
        for (var name in _widgets) {
            let widget = _widgets[name];
            if (!widget || widget.nonInteractive)
                continue;
            handled = widget.onKey.call(widget, e, key);
            if (handled)
                break;
        }
        return handled;
    },

    // Logging with current view info
    log: function () {
        var args = Array.prototype.slice.call(arguments);
        args.splice(0, 0, '(', this.layoutType, this.className, ')');
        var msg = args.join(' ');
        console.log.call(console, msg);
    },

    addWidget: function (name, widget) {
        this._widgets[name] = widget;
        return this;
    },

    removeWidget: function (name) {
        delete this._widgets[name];
        return this;
    },

    getWidget: function (name) {
        return this._widgets[name];
    },

    getWidgets: function () {
        return this._widgets;
    },

    onInit: function (options) {
    },

    // Initialize all widgets
    initWidgets: function () {
        this._widgets = {};
        var widgets = this.widgets;
        for (var key in widgets) {
            var widget = widgets[key];
            var constructor = widget.widgetClass;
            if (!$.isFunction(constructor)) {
                console.log('Invalid widget ' + constructor);
                continue;
            }

            var selector = widget.selector;

            // Create a region for every widget so we can easily show
            this.addRegion(key, selector);

            var options = widget.options;
            this.addWidget(key, new constructor(options));
        }
    },

    // Focus on an element and optionally trigger "focus" event
    focus: function (obj, focusClass, noTrigger) {
        var $obj = (obj instanceof jQuery) ? obj : this.$(obj);
        if ($obj.length > 0) {
            this.log('focus:', '#' + $obj.attr('id'), ', class:', $obj.attr('class'))
            //if focusClas is not specified, anything in nonactive group will be "selected" instead of "active"
            if (!focusClass) {
                if ($obj.parent().hasClass('nonactive'))
                    focusClass = this.selectedClass;
                else
                    focusClass = this.activeClass;
            }
            $obj.addClass(focusClass);
            if(!noTrigger)
                $obj[0].focus();
        }
        return this;
    },

    // Blur an element or elements
    blur: function (obj) {
        if (obj instanceof jQuery) {
            obj.removeClass(this.activeClass + ' ' + this.selectedClass);
            obj.blur();
        }
        else {
            var selector = obj || 'a';
            this.$el.find(selector).removeClass(this.activeClass + ' ' + this.selectedClass);
        }
        return this;
    },

    // Trigger a click on element
    click: function (obj) {
        var $obj = (obj instanceof jQuery) ? obj : this.$(obj);
        if ($obj.length > 0)
            $obj[0].click();
        return $obj;
    },

    // move focus to previous or next item in the list
    moveFocus: function (direction, container, selector, step, focusClass, noFocus) {
        if (!container)
            container = '*';

        selector = selector || this.selector;
        step = step || 1;
        var focusSelector = '.' + this.activeClass + ',.' + this.selectedClass;

        var $container = (container instanceof jQuery) ? container : this.$(container);
        var $items = $container.find(selector).filter(':visible');
        var $curr = $items.filter(focusSelector).first();
        var pos = $items.index($curr);
        var $next = $curr;

        if (pos < 0) {
            $next = $($items.get(0));
        }
        else if (direction == 'prev') {
            $next = $($items.get(pos - step));
            if ($next.length <= 0)
                $next = $($items.get(-1));
        }
        else if (direction == 'next') {
            $next = $($items.get(pos + step));
            if ($next.length <= 0)
                $next = $($items.get(0));
        }

        if(noFocus) {
            return $next;
        }

        // scroll the container if necessary
        if(!this.isVisible($next, $container)) {
            this.scrollTo($next, $container);
        }

        this.blur($curr);
        this.focus($next, focusClass);

        return $next;
    },

    scrollPage: function (direction, container, selector) {
        var $container = (container instanceof jQuery) ? container : this.$(container);
        selector = selector || this.selector;
        var $first = $container.find(selector).not('.back-button').filter(':visible').first();
        var step = 1;

        if ($container.height() < $container[0].scrollHeight) {
            // vertical
            step = parseInt($container.height() / $first.outerHeight(true));
            return this.moveFocus(direction, $container, selector, step);
        }

        if ($container.width() < $container[0].scrollWidth) {
            // horizontal
            step = parseInt($container.width() / $first.outerWidth(true));
            return this.moveFocus(direction, $container, selector, step);
        }
    },

    scrollTo: function ($obj, container) {
        if (!$obj)
            return;

        if (!container)
            container = $obj.parent();

        var $container = (container instanceof jQuery) ? container : this.$(container);
        var margin = 0, position = $obj.position();
        if ($container.width() < $container[0].scrollWidth) { // horizontal
            margin = parseInt($obj.css('marginLeft'));
            if(position.top > 0) { // scroll right
                $container.scrollLeft($container.scrollLeft() + position.left - margin);
            }
            else { // scroll left.  Instead showing the item as the top one, show it at the bottom.
                $container.scrollLeft($container.scrollLeft() + position.left - margin - $container.width() + $obj.width());
            }
        }

        if ($container.height() < $container[0].scrollHeight) { // vertical
            margin = parseInt($obj.css('marginTop'));
            if(position.top > 0) { // scroll down
                $container.scrollTop($container.scrollTop() + position.top - margin);
            }
            else { // scroll up.  Instead showing the item as the top one, show it at the bottom.
                $container.scrollTop($container.scrollTop() + position.top - margin - $container.height() + $obj.height());
            }
        }
    },

    isVisible: function($obj, container) {
        if (!$obj)
            return;

        if (!container)
            container = $obj.parent();
        var $container = (container instanceof jQuery) ? container : this.$(container);

        var position = $obj.position();
        if(position.top < 0 || (position.top + $obj.height()) > $container.height()) {
            return false;
        }
        if(position.left < 0 || (position.left + $obj.width()) > $container.width()) {
            return false;
        }
        return true;
    }

});

export default BaseLayout;